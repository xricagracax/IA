#!/usr/bin/env python
# encoding: utf8
# Artificial Intelligence, UBI 2017
# Modified by: Student name and number

#Imports iniciais
import rospy
from std_msgs.msg import String
from nav_msgs.msg import Odometry

#imports nossos
import math

#Variaveis globais iniciais
x_ant = 0
y_ant = 0
obj_ant = ''

#Variaveis nossas

#Vetor de objetos sala que guarda toda a informação da sala
salas = []
#Sala em que o robo se encontra atualmente
sala_atual = 0
#Objetos encontrados nas salas
objetos_encontrados_salas = {}
#Distancia percorrida pelo robo 
distancia_percorrida = 0.0
#Grafo que tem as ligações entre as salas
G = {}
#Grafo que tem as ligações entre os pontos em que o robo já andou
G_ponto = {}
#Saidas
saidas = {}
#Correspondencia coordenadas -> nome saida
saidas_ = {}
#pontos ja visitados
pontos_visitados = {(0.0,0.0)}
#Area total percorrida
area = 0.0

# ---------------------------------------------------------------
# odometry callback
def callback(data):
	global x_ant, y_ant
	global sala_atual
	global distancia_percorrida
	global area

	x=data.pose.pose.position.x
	y=data.pose.pose.position.y

	sala = qualSala(salas,x,y)

	if sala_atual != sala:
		if sala not in G:
			G[sala] = []
		if sala_atual not in G:
			G[sala_atual] = []

		if sala not in G[sala_atual]:
			G[sala_atual].append(sala)
		if sala_atual not in G[sala]:
			G[sala].append(sala_atual)

		sala_atual = sala

	if ponto_decimal(x_ant,y_ant) not in G_ponto:
		G_ponto[ponto_decimal(x_ant,y_ant)] = [ponto_decimal(x,y)]
	else:
		if ponto_decimal(x,y) not in G_ponto[ponto_decimal(x_ant,y_ant)]:
			G_ponto[ponto_decimal(x_ant,y_ant)].append(ponto_decimal(x,y))
	
	if ponto_decimal(x,y) not in G_ponto:
		G_ponto[ponto_decimal(x,y)] = [ponto_decimal(x_ant,y_ant)]
	else:
		if ponto_decimal(x_ant,y_ant) not in G_ponto[ponto_decimal(x,y)]:
			G_ponto[ponto_decimal(x,y)].append(ponto_decimal(x_ant,y_ant))
		

	# show coordinates only when they change
	if x != x_ant or y != y_ant:
		print " x=%.1f y=%.1f sala=%d " % (x,y,sala_atual)
	
	ponto_A = ponto_decimal(x,y)
	if ponto_A not in pontos_visitados:
		pontos_visitados.add((ponto_A[0],ponto_A[1]))
		area+= 0.1*0.1

	#Calcular diferenca da distancia entre os pontos
	#e adicionar para obter a distância percorrida
	distancia_percorrida += math.sqrt((x-x_ant)*(x-x_ant)+(y-y_ant)*(y-y_ant))

	x_ant = x
	y_ant = y

# ---------------------------------------------------------------
# object_recognition callback
def callback1(data):
	global obj_ant
	global sala_atual
	obj = data.data

	if obj != obj_ant:
		print "object is %s" % data.data
		if(sala_atual != -1 and data.data != ""):
			salas[sala_atual-1].introduzir_objetos(data.data)
	obj_ant = obj
		
# ---------------------------------------------------------------
# questions_keyboard callback
def callback2(data):
	global sala_atual
	global x_ant, y_ant	
	global G_ponto
	global salas
	global saidas_ 
	global area

	print "question is %s" % data.data

	if data.data == '1':
		if sala_atual != -1:
			salas[sala_atual-1].tipo_de_sala()

	if data.data == '2':
		if 'ana' in objetos_encontrados_salas:
			if salas[objetos_encontrados_salas['ana'][2]-1].n_book>0:
				print 'A Ana gosta de ler'
			else:
				print 'A Ana não gosta de ler'
		else:
			print 'Ainda não foi encontrada a Ana'

	if data.data == '3':
		if salas[4].entrou:
			caminho = breadth_first_search(G,sala_atual,5)

			if caminho[0] == 5:
				print 'Já se encontra na sala 5, não precisa de passar por outra sala'

			else:
				for i in caminho:
					print str(i) + " -> ",

				print 'Precisa de passar por ' + str(len(caminho)-1) + " sala(s)"
		else:
			print 'Ainda não descobriu a sala 5'

	if data.data == '4':
		if saidas == {}:
			print 'Ainda não foi descoberta uma saida'
		else:
			inicio = ponto_decimal(x_ant,y_ant)
			cam = a_star(G_ponto,inicio)
			C = path(cam[0],inicio,cam[1])
			exit = saidas_ [cam[1]]
			path_de_salas(C,exit)

	if data.data == 5:
		n_salas_com_pessoas=0
		n_salas_com_pessoas_cadeiras=0

		for i in range(10):
			sala_com_pessoas = salas[i].com_pessoas()
			if (sala_com_pessoas[0] == True):
				print 'Sala ' + str(i+1) + ' tem pessoas!'
				n_salas_com_pessoas += 1
				if sala_com_pessoas[1]:
					print 'Sala ' + str(i+1) + ' com uma cadeira'
					n_salas_com_pessoas_cadeiras += 1

		if n_salas_com_pessoas != 0:
			print 'A probabilidade é de '+str((float(n_salas_com_pessoas_cadeiras)/float(n_salas_com_pessoas)))
		else:
			print 'Ainda não encontrou nenhuma sala com pessoas, probabilidade não definida!'		

	if data.data == '6':
		n_salas_sem_pessoas_cadeiras = 0
		n_salas_sem_pessoas_cadeiras_com_1mesa = 0

		for i in range(10):
			sala_sem_pessoas_cadeiras = salas[i].sem_pessoas_sem_cadeiras()
			if (sala_sem_pessoas_cadeiras[0] == True):
				print 'Sala ' + str(i+1) + ' sem pessoas e sem cadeiras'
				n_salas_sem_pessoas_cadeiras += 1
				if sala_sem_pessoas_cadeiras[1]:
					print 'Sala ' + str(i+1) + ' com uma mesa'
					n_salas_sem_pessoas_cadeiras_com_1mesa += 1

		if n_salas_sem_pessoas_cadeiras != 0:
			print 'A probabilidade é de '+str((float(n_salas_sem_pessoas_cadeiras_com_1mesa)/float(n_salas_sem_pessoas_cadeiras)))
		else:
			print 'Ainda não encontrou nenhuma sala sem cadeiras e sem pessoas, probabilidade não definida!'

	if data.data == '7':
		time = rospy.get_rostime().secs
		print 'Distância percorrida = %.5f' % distancia_percorrida
		print 'Tempo = %.5f' % time
		print 'Velocidade Media: x = %.5f ; y = %.5f' % (x_ant/time, y_ant/time)
		print 'Rapidez Média: v = %.5f' % (distancia_percorrida/(time))

	if data.data == '8':
		time = rospy.get_rostime().secs
		n_salas_com_mesas_sem_pessoas = numeroDeSalasComMesasSemPessoas(salas)
		n_pessoas = numeroPessoas(salas)
		Et = areaTotal(salas)
		E = area

		print 'Numero de pessoas encontradas: ' + str(n_pessoas)
		print 'Espaco observado: ' + str(E)
		print 'Espaco total : ' + str(Et)
		print 'Tempo : ' + str(time)
		print 'Numero de salas com mesas e sem pessoas : ' + str(n_salas_com_mesas_sem_pessoas)

		print "Existem " + str(n_salas_com_mesas_sem_pessoas) + " salas com mesas e sem pessoas."
		print "Estima-se que encontre " + str(int(math.floor((1.0-E/Et)*((n_pessoas+n_salas_com_mesas_sem_pessoas)*60.0/float(time))))) + " pessoas no próximo minuto."

# ---------------------------------------------------------------
def agent():
	rospy.init_node('agente')

	rospy.Subscriber("questions_keyboard", String, callback2)
	rospy.Subscriber("object_recognition", String, callback1)
	rospy.Subscriber("odom", Odometry, callback)

	rospy.spin()

# ---------------------------------------------------------------
#Definição da classe sala, definida por dois pontos (retângulo) e o número
#INICIO DA CLASSE------------------------------------------------
class Sala:

	def __init__(self,x1,y1,x2,y2,n):
		#Sala do objeto
		self.n = n	
		
		#Coordenadas da sala	
		self.x1 = x1
		self.y1 = y1
 		self.x2 = x2
 		self.y2 = y2   
 		
		#Número de objetos
		self.n_chair = 0
		self.n_book = 0
		self.n_table = 0
		self.n_computer = 0
		self.n_person = 0
		self.n_door = 0
		
		#Lista de objetos encontrados nesta sala
		self.objetos_da_sala = []
		#Ja entrou nesta sala
		self.entrou = False

	#Ver se um ponto (x,y) se encontra dentro desta sala
	def dentro(self,x,y):
		if (self.x1 > x) and (x > self.x2):
			if (self.y1 < y) and (y < self.y2):
				return True
		return False 

	#Adicionar objeto ao contador
	def adicionar_objeto(self,objeto_nome):
		if objeto_nome == 'chair':
			self.n_chair += 1
		elif objeto_nome == 'book':
			self.n_book += 1
		elif objeto_nome == 'table':
			self.n_table += 1
		elif objeto_nome == 'computer':
			self.n_computer += 1
		elif objeto_nome == 'door': 
			self.n_door += 1
		elif objeto_nome == 'person': 
			self.n_person += 1
		else:
			print 'Objeto não identificado' 

	#Introduzir objetos encontrados nesta sala
	def introduzir_objetos(self,objetos_encontrados):
		global saidas
		global saidas_

		objetos = objetos_encontrados.split(',')
		
		for objeto in objetos:

			objeto_nome = objeto.partition('_')

			if objeto_nome[0] == 'door':
				if saidas == {}:
					saidas = {ponto_decimal(x_ant,y_ant)}
					saidas_[ponto_decimal(x_ant,y_ant)] = objeto_nome[2]
				else:				
					saidas.add(ponto_decimal(x_ant,y_ant))
					saidas_[ponto_decimal(x_ant,y_ant)] = objeto_nome[2]

			#Ver se este objeto ja foi encontrado noutra sala
			if objeto_nome[2] in objetos_encontrados_salas:
				#Se já tiver encontrado noutra sala faz o qualSala do ponto medio entre os dois pontos (Ponto atual e Ponto onde foi visto o objeto)
				sala_media = qualSala(salas,(objetos_encontrados_salas[objeto_nome[2]][0]+x_ant)/2,(objetos_encontrados_salas[objeto_nome[2]][1]+y_ant)/2)
				#Caso a sala media seja igual à sala onde o objeto foi visto anteriormente, não faz nada
				if sala_media == objetos_encontrados_salas[objeto_nome[2]][2]:
					#Caso a sala deste objeto nao corresponda ao 
					if self.n != objetos_encontrados_salas[objeto_nome[2]][2]:
						print 'O objeto '+objeto_nome[2]+' encontra-se noutra sala'
					#Caso a sala deste objeto seja a mesma
					else:
						print 'O objeto '+objeto_nome[2]+' já foi adicionada anteriormente'
					continue
				else:
					#Remover objeto da sala onde foi visto anteriormente e adicionar a esta, assim atualizando bem os objetos pertencentes a cada sala
					print 'Remover '+objeto_nome[2]+' da sala ' + str(objetos_encontrados_salas[objeto_nome[2]][2])+ ' uma vez que esta dentro da sala '+str(self.n) 
					print 'Adicionar '+objeto_nome[2]+' à sala '+str(self.n)
					salas[objetos_encontrados_salas[objeto_nome[2]][2]-1].remover_objeto(objeto)
					self.adicionar_objeto(objeto_nome[0])					
					objetos_encontrados_salas[objeto_nome[2]]=((x_ant,y_ant,self.n))
					self.objetos_da_sala.append(objeto_nome[2])
			else:			
				#Adiciona objeto à Sala
				self.adicionar_objeto(objeto_nome[0])
				print 'Adicionar '+objeto_nome[2]+' à sala '+str(self.n) 
				objetos_encontrados_salas[objeto_nome[2]]=((x_ant,y_ant,self.n))
				self.objetos_da_sala.append(objeto_nome[2])

    #Remover objeto nesta sala
	def remover_objeto(self,objeto_encontrado):
		#Caso encontre mais de um objeto ao mesmo tempo
		objetos = objeto_encontrado.split(',')

		#Percorrer objetos encontrados e remove-los desta sala
		for objeto in objetos:
			
			objeto_nome = objeto.partition('_')
			if objeto_nome[2] not in self.objetos_da_sala:
				print 'Objeto '+objeto_nome[2]+' não se encontra na sala '+str(self.n) 
				continue 
			else:
				self.objetos_da_sala.remove(objeto_nome[2])
				if objeto_nome[0] == 'chair':
					self.n_chair -= 1
				elif objeto_nome[0] == 'book':
					self.n_book -= 1
				elif objeto_nome[0] == 'table':
					self.n_table -= 1
				elif objeto_nome[0] == 'computer':
					self.n_computer -= 1
				elif objeto_nome[0] == 'door': 
					self.n_door -= 1
				elif objeto_nome[0] == 'person': 
					self.n_person -= 1
				else:
					print 'Objeto não identificado'

	#Imprime o tipo de sala
	def tipo_de_sala(self):

		print 'Número de blocos '+str(self.n_chair+self.n_book+self.n_table+self.n_computer+self.n_person+self.n_door)
		print 'Número de cadeiras '+str(self.n_chair)
		print 'Número de livros '+str(self.n_book)
		print 'Número de mesas '+str(self.n_table)
		print 'Número de computadores '+str(self.n_computer)
		print 'Número de pessoas '+str(self.n_person)
		print 'Número de portas '+str(self.n_door)

		if self.n_chair > 0 and self.n_book == 0 and self.n_table == 0 and self.n_computer == 0:
			print 'Sala de Espera'
		elif self.n_chair > 0 and self.n_book > 0 and self.n_table > 0 and self.n_computer == 0:
			print 'Sala de Estudo!'
		elif self.n_computer > 0 and self.n_table > 0 and self.n_chair > 0:
			print 'Laboratório de Informática!'
		elif self.n_table == 1 and self.n_chair > 0 and self.n_book == 0 and self.n_computer == 0:
			print 'Sala de Reuniões!'
		else:
			print 'Sala genérica!'
		
	#Devolver se é uma sala sem cadeiras e sem pessoas
	def sem_pessoas_sem_cadeiras(self):
		#Se não tiver pessoas nem cadeiras, devolver true e ainda devolver se o numero de mesas == 1
		if self.n_person == 0 and self.n_chair == 0 and self.entrou:
			#Se ambos forem verdadeira devolve (True,True), caso contrário devolve (True,False)
			#Ao devolver False no n_table == 1 não incrementa o numerador, mas o denominador incrementa
			return (True,self.n_table == 1)
		return (False,False)

	def area_sala(self):
		return abs((self.x1) - (self.x2))*abs((self.y1) - (self.y2))


		#Devolver se é uma sala com uma pessoa
	def com_pessoas(self):
		#Se tiver pessoas , devolver true 
		if self.n_person >= 1 and self.entrou:
			#Se ambos forem verdadeira devolve (True,True), caso contrário devolve (True,False)
			return (True, self.n_chair >= 1 )
		return (False,False)


		
#FIM DA CLASSE---------------------------------------------------

#Devolve o numero de pessoas encontradas
def numeroPessoas(lista_de_salas):
	soma = 0.0
	for i in range(len(lista_de_salas)):
		soma += lista_de_salas[i].n_person

	return soma

def numeroDeSalasComMesasSemPessoas(lista_de_salas):
	soma = 0.0
	for i in range(len(lista_de_salas)):
		if lista_de_salas[i].n_person == 0 and lista_de_salas[i].n_table > 0:
			soma += 1

	return soma

def areaTotal(lista_de_salas):
	soma = 0.0

	for sala in lista_de_salas:
		soma += sala.area_sala() 

	return soma

#Função que devolve em que sala se encontra o robô
def qualSala(lista_de_salas,x,y):
	
	for i in range(len(lista_de_salas)):
		if lista_de_salas[i].dentro(x,y):
			lista_de_salas[i].entrou = True
			return lista_de_salas[i].n
			
	return -1

# ---------------------------------------------------------------

#Retorna um ponto com parte decimal de tamanho 1
def ponto_decimal(x,y):
	aux_x = float("{0:.1f}".format(x))
	aux_y = float("{0:.1f}".format(y))
	return (aux_x,aux_y)

#Ex 4
def h(x,y):

	global saidas

	ponto_mais_proximo = (0,0)
	ponto_atual = ponto_decimal(x,y)
	minimo = 1000000.0

	for saida in saidas:
		if distancia(ponto_atual,saida) < minimo:
			ponto_mais_proximo = saida
			minimo = distancia(ponto_atual,saida)

	return (ponto_mais_proximo,minimo)
		

def distancia(a,b):

	return math.sqrt((a[0]-b[0])*(a[0]-b[0]) + (a[1]-b[1])*(a[1]-b[1]))
	
#Devolve um caminho de pontos
def path(pDict,origem,destino):

	C = []

	#predecessores
	if len(pDict) != 0:
		while(origem != pDict[destino]):
      			C.append(pDict[destino])
      			destino = pDict[destino]

		C.append(origem)

	return C[::-1]

def path_de_salas(caminho,exit):

	b = -1

	print caminho

	print "*CAMINHO",
	for ponto in caminho:
		if b != qualSala(salas,ponto[0],ponto[1]):			
			b = qualSala(salas,ponto[0],ponto[1])
			if b > 0:
				print str(b) + " -> ",

	print exit+"*"
		

#O algoritmo A* para encontrar o caminho mais curto
def a_star(G, inicio):
  
	global saidas

	#predecessor
	pDict = {}
	#custo de distancia
	gDict = {inicio : 0}
	#custo da distancia mais a heuristica
	fDict = {inicio : h(inicio[0],inicio[1])[1]}
	#conjunto de pontos a investigar
	openSet = [inicio]
	#conjunto de pontos já investigados
	closedSet = []

	while len(openSet) != 0:
    
		lowest = 1000000.0
		node = -1

		for i in range(len(openSet)):
			print str(fDict[openSet[i]])+" < "+str(lowest)
			if fDict[openSet[i]] < lowest:
				lowest = fDict[openSet[i]]
				node = openSet[i]
			if node in saidas:
				return (pDict,node)
		
		closedSet.append(node)
    		openSet.remove(node)
    
		nbr = G[node]

		for i in range(len(nbr)):
			if nbr[i] in closedSet:
				continue
			if nbr[i] not in openSet:
				openSet.append(nbr[i])
				gDict[nbr[i]] = 1000000.0
			if gDict[nbr[i]] > gDict[node] + distancia(node,nbr[i]):
				gDict[nbr[i]] = gDict[node] + distancia(node,nbr[i])
				fDict[nbr[i]] = gDict[nbr[i]] + h(nbr[i][0],nbr[i][1])[1]
				pDict[nbr[i]] = node

	return {-1};

#Ex 3
#Algoritmo bfd -> Pesquisa Primeiro em Largura
def breadth_first_search(G, inicial, final):

    fila = []
    #Por a sala atual no inicio
    fila.append([inicial])
    while fila:
        # Buscar o primeiro elemento da fila
        caminho = fila.pop(0)
        # Ir buscar o ultimo do caminho
        sala = caminho[-1]
        # Caminho encontrado, retornar
        if sala == final:
            return caminho
        # Ver nós adjacentes e construir um novo caminho
        for adjacente in G.get(sala, []):
            caminho_novo = list(caminho)
            caminho_novo.append(adjacente)
            fila.append(caminho_novo)

if __name__ == '__main__':

	salas.append(Sala(3.7,-3.2,-1.0,1.5,1))
	salas.append(Sala(-1.0,-3.2,-6.0,1.5,2)) 
	salas.append(Sala(-6.0,-3.2,-15.7,1.5,3)) 
	salas.append(Sala(3.7,1.5,-1.0,6.5,4))
	salas.append(Sala(-1.0,1.5,-6.0,11.1,5))
	salas.append(Sala(-6.0,1.5,-11.0,6.5,6))
	salas.append(Sala(-11.0,1.5,-15.7,6.5,7))
	salas.append(Sala(3.7,6.5,-1.0,11.2,8))
	salas.append(Sala(-6.0,6.5,-11.0,11.2,9))
	salas.append(Sala(-11.0,6.5,-15.7,11.2,10))

	agent()
